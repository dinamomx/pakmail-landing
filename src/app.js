import Vue from '../node_modules/vue/dist/vue.common';
import FormGeneric from './FormGeneric.vue';
import InfiniteSlider from './InfiniteSlider.vue';

Vue.component(FormGeneric.name, FormGeneric);
Vue.component(InfiniteSlider.name, InfiniteSlider);

Document.prototype.ready = callback => {
  const stateIsReady = state => state === 'interactive' || state === 'complete';
  if (callback && typeof callback === 'function') {
    if (stateIsReady(document.readyState)) {
      return callback();
    }
    document.addEventListener('DOMContentLoaded', () => {
      if (stateIsReady(document.readyState)) {
        return callback();
      }
    });
  }
};
document.ready(() => {
  const app = new Vue({
    el: '#app',
    data: {}
  });

  window.$app = app;
});
