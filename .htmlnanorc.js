module.exports = {
  mergeStyles: false,
  minifySvg: false,
  minifyJs: {
    minify: {
      mangle: false
    },
    keep_fnames: true,
    mangle: false
  },
  minifyJson: false,
  mergeScripts: false
};
